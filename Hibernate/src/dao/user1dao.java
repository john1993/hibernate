package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import vo.user1vo;


public class user1dao {
	public void insert(user1vo r){
		try
		{
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			
			Session session =sessionFactory.openSession();
			
			Transaction tr = session.beginTransaction();
			
			session.save(r);
			
			tr.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
