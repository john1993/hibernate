package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.user1dao;
import dao.userdtldao;

import vo.user1vo;
import vo.userdtlvo;

/**
 * Servlet implementation class RegController
 */
@WebServlet("/RegController")
public class RegController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String uname = request.getParameter("uname");
		String pwd = request.getParameter("pwd");
		
		user1vo v1 = new user1vo();
		v1.setUname(uname);
		v1.setPwd(pwd);
		
		user1dao d1 = new user1dao();
		d1.insert(v1);
		
		
		userdtlvo u1 = new userdtlvo();
		u1.setFname(fname);
		u1.setLname(lname);
		u1.setV1(v1);
		

		userdtldao u = new userdtldao();
		u.insert(u1);
		
		response.sendRedirect("reg.jsp");
		
		
	}

}
