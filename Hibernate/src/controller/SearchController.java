package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vo.uservo;
import dao.searchdao;


/**
 * Servlet implementation class SearchController
 */
@WebServlet("/SearchController")
public class SearchController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String fname = request.getParameter("fname");
		
		
		System.out.println("name = == " + fname);
		uservo r=new uservo();
		r.setFname(fname);
		
		List ls = new ArrayList();
		
		searchdao r1=new searchdao();
		ls = r1.search(r);
		
		HttpSession s = request.getSession();
		s.setAttribute("ls", ls);
		System.out.println("size = = = = = = " + ls.size());
		
		
		
	
		s.setAttribute("insert", "Data Inserted Sucessfully");
		
		response.sendRedirect("search.jsp");
	}

}
