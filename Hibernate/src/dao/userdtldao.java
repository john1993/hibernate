package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import vo.userdtlvo;


public class userdtldao {
	
	public void insert(userdtlvo r){
		try
		{
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			
			Session session =sessionFactory.openSession();
			
			Transaction tr = session.beginTransaction();
			
			session.save(r);
			
			tr.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
